import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

import com.app.frame 1.0

Window {
    visible: true
    width: Screen.width
    height: Screen.height

    Item {
        id: cameraSpace
        width: parent.width
        height: parent.height*0.7
        anchors.top: parent.top

        RowLayout{
            anchors.fill: parent

            Item{
                Layout.preferredWidth: parent.width*0.5
                Layout.preferredHeight: parent.height

                Rectangle{
                    width: parent.width*0.9
                    height: parent.height*0.7
                    anchors.centerIn: parent
                    color: "grey"
                }

                Frame{
                    id: leftCamera
                    width: parent.width*0.9
                    height: parent.height*0.7
                    anchors.centerIn: parent
                    visible: false
                }


                Button{
                    id: leftCameraButton
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Select Left Camera")
                    onClicked: {
                        availableCameras.getCamerasInfo()
                        availableCamerasDialog.open()
                    }
                }
            }
            Item{
                Layout.preferredWidth: parent.width*0.5
                Layout.preferredHeight: parent.height

                Rectangle{
                    width: parent.width*0.9
                    height: parent.height*0.7
                    anchors.centerIn: parent
                    color: "grey"

                }

                Frame{
                    id: rightCamera
                    width: parent.width*0.9
                    height: parent.height*0.7
                    anchors.centerIn: parent
                    visible: false
                }

                Button{
                    id: rightCameraButton
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Select Right Camera")
                    onClicked: {
                        availableCameras.getCamerasInfo()
                        availableCamerasDialog.open()
                    }
                }
            }
        }
    }

    Item{
        id: controlSpace
        width: parent.width
        height: parent.height*0.3
        anchors.top: cameraSpace.bottom
        RowLayout{
            anchors.fill: parent
            Item{
                Layout.preferredWidth: parent.width*0.4
                Layout.preferredHeight: parent.height

                Button{
                    anchors.centerIn: parent
                    text: qsTr("Start Calibration")
                }

            }
            Item{
                Layout.preferredWidth: parent.width*0.6
                Layout.preferredHeight: parent.height



            }
        }
    }

    Connections{
        target: videoThread
        onUpdateRightFrame: rightCamera.setRawFrame(frame)
    }

    Connections{
        target: videoThread
        onUpdateLeftFrame: leftCamera.setRawFrame(frame)
    }


    Dialog{
        id: availableCamerasDialog
        title: qsTr("Available Cameras")
        x: (parent.width - width)/2
        y: (parent.height - height)/2
        width: parent.width*0.8
        height: parent.height*0.6
        standardButtons: Dialog.Ok

        header: Rectangle{
            color: "green"
            width: parent.width
            height: parent.height*0.2

            Text {
                text: availableCamerasDialog.title
                font.pointSize: 14
                anchors.centerIn: parent
                color: "white"
            }
        }

        contentItem: Item {
            width: parent.width
            height: parent.height*0.8

            ComboBox{
                width: parent.width*0.8
                anchors.centerIn: parent
                displayText: qsTr("Choose a Camera")
                model: availableCameras
                textRole: "deviceName"
                onHighlighted: {
                    displayText = availableCameras.getDeviceName(highlightedIndex)
                }
            }

        }

         onAccepted: {
            //ALGO MARAVILLOSO PASA
         }
    }
}
