#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "src/cameralist.h"
#include "src/threadmanager.h"
#include "src/frame.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    CameraList availableCameras;

    QQmlApplicationEngine engine;

    threadManager videoThread;

    qmlRegisterType<Frame>("com.app.frame", 1, 0, "Frame");
    qRegisterMetaType<cv::Mat>("Mat");

    engine.rootContext()->setContextProperty("videoThread", &videoThread);
    engine.rootContext()->setContextProperty("availableCameras", &availableCameras);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
