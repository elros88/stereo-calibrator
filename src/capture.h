﻿#ifndef CAPTURE_H
#define CAPTURE_H

#include <QObject>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>
#include <opencv2/calib3d.hpp>

using namespace cv;

#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

class Capture : public QObject
{
    Q_OBJECT

    VideoCapture *videoCapture1;
    VideoCapture *videoCapture2;

    QString url;
    bool stopVideo;
    int device1;
    int device2;

public:
    explicit Capture(QObject *parent = nullptr);

//    void setUrl(QString &value);
//    void findObjects(Mat &frame, vector<Mat> &outs, Net &net,
//                     vector<string> classes);
//    void drawObjectBox(int classId, int left, int top, int right, int bottom,
//                       Mat &frame, vector<string> &classes);

    void setDevice(int value);

//    bool searchPattern(Mat &frame, vector<Point2f> &pointBuf, Size &boardSize);
//    bool calibrate(Mat &cameraMatrix, Mat &distCoeffs, vector<vector<Point2f> > imagePoints,
//                   vector<Mat> &rvecs, vector<Mat> &tvecs, vector<float> &reprojErrs,
//                   double &totalAvgErr, vector<Point3f> &newObjPoints, int
//                   &squareSize, Size &boardSize, Size &imageSize);

//    void saveCameraParams(Size &imageSize, Mat &cameraMatrix, Mat &distCoeffs);
//    void openCameraInfo(Mat &cameraMatrix1, Mat &cameraMatrix2,
//                        Mat &distCoeffs1, Mat &distCoeffs2,
//                        Size &imageSize1, Size &imageSize2);


    void setDevice1(int value);

    void setDevice2(int value);

signals:

    void newFrameCaptured(Mat frame);
    void newFrame1Captured(Mat frame);
    void newFrame2Captured(Mat frame);

public slots:

    void stereoCalibrate();
//    bool openVideo();
//    void substractBackground();
//    void detectColor();
//    void openCamera();
//    void detectFaces();
//    void trackYolo();
//    void openCameraByNum();
    void stopCapture();
//    void stereoMap();

};

#endif // CAPTURE_H
