#include "capture.h"

//void Capture::openCameraByNum()
//{
//    Mat rawFrame;
//    vector<vector<Point2f> > imagePoints;
//    vector<Point2f> pointBuf;
//    vector<Point3f> newObjPoints;
//    Mat cameraMatrix;
//    Mat distCoeffs;
//    vector<Mat> rvecs;
//    vector<Mat> tvecs;
//    vector<float> reprojErrs;
//    double totalAvgErr;
//    Size boardSize = Size(7, 7);
//    int squareSize = 2;
//    Size imageSize = Size(300, 200);
//    bool found = false;

//    videoCapture = new VideoCapture;
//    videoCapture->open(device);

//    if(!videoCapture->isOpened())
//    {
//        return;
//    }

//    while(videoCapture->read(rawFrame) && !stopVideo)
//    {
//        resize(rawFrame, rawFrame, Size(300, 200), 0, 0, INTER_CUBIC);

//        if(searchPattern(rawFrame, pointBuf, boardSize))
//        {
//            imagePoints.push_back(pointBuf);
//            found = true;
//        }

//        emit(newFrameCaptured(rawFrame));
//    }

//    videoCapture->release();

//    if(found)
//    {
//        if(calibrate(cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs,
//                     reprojErrs, totalAvgErr, newObjPoints,
//                     squareSize, boardSize, imageSize))
//        {
//            saveCameraParams(imageSize, cameraMatrix, distCoeffs);
//        }
//    }

//}

void Capture::stopCapture()
{
    stopVideo = true;
}

void Capture::stereoCalibrate()
{
    Mat rawFrame1;
    Mat rawFrame2;

    videoCapture1 = new VideoCapture;
    videoCapture2 = new VideoCapture;

    videoCapture1->open(device1);
    videoCapture2->open(device2);

    if(!videoCapture1->isOpened() ||
       !videoCapture2->isOpened())
    {
        return;
    }

    while(videoCapture1->read(rawFrame1) && videoCapture2->read(rawFrame2))
    {
        emit newFrame1Captured(rawFrame1);
        emit newFrame2Captured(rawFrame2);
    }
}

//void Capture::stereoMap()
//{
//    Mat rawFrame1;
//    Mat rawFrame2;
//    Mat cameraMatrix1;
//    Mat cameraMatrix2;
//    Mat distCoeffs1;
//    Mat distCoeffs2;
//    Mat map11;
//    Mat map12;
//    Mat map21;
//    Mat map22;
//    Mat tempFrame1;
//    Mat tempFrame2;
//    Mat depthMap;
//    Size imageSize2;
//    Size imageSize1;

//    openCameraInfo(cameraMatrix1, cameraMatrix2,
//                   distCoeffs1, distCoeffs2,
//                   imageSize1, imageSize2);

//    videoCapture1 = new VideoCapture;
//    videoCapture2 = new VideoCapture;

//    videoCapture1->open(device1);
//    videoCapture2->open(device2);

//    if(!videoCapture1->isOpened() ||
//       !videoCapture2->isOpened())
//    {
//        return;
//    }

//    while (videoCapture1->read(rawFrame1) && videoCapture2->read(rawFrame2))
//    {
//        resize(rawFrame1, rawFrame1, imageSize1, 0, 0, INTER_CUBIC);
//        resize(rawFrame2, rawFrame2, imageSize2, 0, 0, INTER_CUBIC);

//        GaussianBlur(rawFrame1, rawFrame1, Size(31, 31), 0, 0);
//        GaussianBlur(rawFrame2, rawFrame2, Size(31, 31), 0, 0);

//        initUndistortRectifyMap(cameraMatrix1, distCoeffs1, Mat(),
//                 getOptimalNewCameraMatrix(cameraMatrix1, distCoeffs1,
//                                           imageSize1, 1, imageSize1, 0),
//                                imageSize1, CV_16SC2, map11, map12);

//        initUndistortRectifyMap(cameraMatrix2, distCoeffs2, Mat(),
//                 getOptimalNewCameraMatrix(cameraMatrix2, distCoeffs2,
//                                           imageSize2, 1, imageSize2, 0),
//                                imageSize2, CV_16SC2, map21, map22);

//        remap(rawFrame1, tempFrame1, map11, map12, INTER_LINEAR);
//        remap(rawFrame2, tempFrame2, map21, map22, INTER_LINEAR);

//        Ptr<StereoSGBM> sgbm = StereoSGBM::create(5, 16, 3);
//        int sgbmWinSize = 3;
//        int cn = rawFrame1.channels();
//        sgbm->setPreFilterCap(100);
//        sgbm->setBlockSize(3);
//        sgbm->setP1(16*cn*sgbmWinSize*sgbmWinSize);
//        sgbm->setP2(32*cn*sgbmWinSize*sgbmWinSize);
//        sgbm->setMinDisparity(5);
//        sgbm->setNumDisparities(16);
//        sgbm->setUniquenessRatio(10);
//        sgbm->setSpeckleWindowSize(100);
//        sgbm->setSpeckleRange(32);
//        sgbm->setDisp12MaxDiff(1);
//        sgbm->setMode(StereoSGBM::MODE_SGBM);

//        sgbm->compute(rawFrame1, rawFrame2, depthMap);

//        depthMap.convertTo(depthMap, CV_8U, 255/(sgbm->getNumDisparities()*16.));

//        emit newFrameCaptured(depthMap);
//    }

//}

//bool Capture::searchPattern(Mat &frame, vector<Point2f> &pointBuf, Size &boardSize)
//{
//    bool found = findChessboardCorners(frame, boardSize, pointBuf);
//    if(found)
//    {
//        Mat viewGray;
//        cvtColor(frame, viewGray, COLOR_RGB2GRAY);
//        cornerSubPix(viewGray, pointBuf, Size(11, 11), Size(-1, -1),
//                     TermCriteria(TermCriteria::EPS+TermCriteria::COUNT, 30, 0.0001));

//        drawChessboardCorners(frame, boardSize, Mat(pointBuf), found);
//    }

//    return found;
//}

//bool Capture::calibrate(Mat &cameraMatrix, Mat &distCoeffs,
//                            vector<vector<Point2f> > imagePoints, vector<Mat> &rvecs,
//                            vector<Mat> &tvecs, vector<float> &reprojErrs,
//                            double &totalAvgErr, vector<Point3f> &newObjPoints,
//                            int &squareSize, Size &boardSize, Size &imageSize)
//{

//    cameraMatrix = Mat::eye(3, 3, CV_64F);
//    distCoeffs = Mat::zeros(8, 1, CV_64F);
//    vector<vector<Point3f> > objectPoints(1);
//    float grid_width = squareSize * (boardSize.width - 1);

//    for( int i = 0; i < boardSize.height; ++i )
//    {
//        for( int j = 0; j < boardSize.width; ++j )
//        {
//            objectPoints[0].push_back(Point3f(j*squareSize, i*squareSize, 0));
//        }
//    }

//    objectPoints[0][boardSize.width - 1].x = objectPoints[0][0].x + grid_width;
//    newObjPoints = objectPoints[0];
//    objectPoints.resize(imagePoints.size(),objectPoints[0]);

//    calibrateCameraRO(objectPoints, imagePoints, imageSize, -1,
//                          cameraMatrix, distCoeffs, rvecs, tvecs, newObjPoints);

//    bool ok =checkRange(cameraMatrix) && checkRange(distCoeffs);

//    objectPoints.clear();
//    objectPoints.resize(imagePoints.size(), newObjPoints);

//    vector<Point2f> imagePoints2;
//    size_t totalPoints = 0;
//    double totalErr = 0, err;
//    reprojErrs.resize(objectPoints.size());

//    for(size_t i = 0; i < objectPoints.size(); ++i )
//    {
//        projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix,
//                      distCoeffs, imagePoints2);

//        err = norm(imagePoints[i], imagePoints2, NORM_L2);

//        size_t n = objectPoints[i].size();
//        reprojErrs[i] = (float) sqrt(err*err/n);
//        totalErr        += err*err;
//        totalPoints     += n;
//    }

//     totalAvgErr = sqrt(totalErr/totalPoints);

//     return ok;
//}

//void Capture::saveCameraParams(Size &imageSize, Mat &cameraMatrix, Mat &distCoeffs)
//{
//    FileStorage fs("/home/antonio/app/camera"+to_string(device), FileStorage::WRITE);

//    fs<<"image_width"<<imageSize.width;
//    fs<<"image_height"<<imageSize.height;
//    fs<<"camera_matrix"<<cameraMatrix;
//    fs<<"distortion_coefficients"<<distCoeffs;

//    fs.release();
//}

//void Capture::openCameraInfo(Mat &cameraMatrix1, Mat &cameraMatrix2,
//                                 Mat &distCoeffs1, Mat &distCoeffs2,
//                                 Size &imageSize1, Size &imageSize2)
//{
//    FileStorage fs1("/home/antonio/app/camera0", FileStorage::READ);
//    FileStorage fs2("/home/antonio/app/camera1", FileStorage::READ);

//    fs1["image_width"] >> imageSize1.width;
//    fs1["image_height"] >> imageSize1.height;
//    fs1["camera_matrix"] >> cameraMatrix1;
//    fs1["distortion_coefficients"] >> distCoeffs1;
//    fs1.release();

//    fs2["image_width"] >> imageSize2.width;
//    fs2["image_height"] >> imageSize2.height;
//    fs2["camera_matrix"] >> cameraMatrix2;
//    fs2["distortion_coefficients"] >> distCoeffs2;
//    fs2.release();
//}

//void Capture::findObjects(Mat &frame, vector<Mat> &outs, Net &net, vector<string> classes)
//{
//    vector<int> outLayers = net.getUnconnectedOutLayers();
//    vector<int> classIds;
//    vector<float> confidences;
//    vector<Rect> boxes;

//    float confThreshold = 0.5;
//    float nmsThreshold = 0.4;

//    for(unsigned int i = 0; i < outs.size(); i++)
//    {
//        float* data = (float*) outs[i].data;
//        for(unsigned int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
//        {
//            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
//            Point classIdPoint;
//            double confidence;
//            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
//            if (confidence > confThreshold)
//            {
//                int centerX = (int)(data[0] * frame.cols);
//                int centerY = (int)(data[1] * frame.rows);
//                int width = (int)(data[2] * frame.cols);
//                int height = (int)(data[3] * frame.rows);
//                int left = centerX - width / 2;
//                int top = centerY - height / 2;

//                classIds.push_back(classIdPoint.x);
//                confidences.push_back((float)confidence);
//                boxes.push_back(Rect(left, top, width, height));
//            }
//        }
//    }

//    vector<int> indices;
//    NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
//    for (size_t i = 0; i < indices.size(); ++i)
//    {
//        int idx = indices[i];
//        Rect box = boxes[idx];
//        drawObjectBox(classIds[idx], box.x, box.y,
//                      box.x + box.width, box.y + box.height, frame, classes);
//    }

//}

//void Capture::drawObjectBox(int classId, int left, int top, int right, int bottom, Mat &frame, vector<string> &classes)
//{
//    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0,0,255));
//    string label = classes[classId];

//    putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5,
//            Scalar(255,255,0));
//}

void Capture::setDevice1(int value)
{
    device1 = value;
}

void Capture::setDevice2(int value)
{
    device2 = value;
}

Capture::Capture(QObject *parent) : QObject(parent)
{
    stopVideo = false;
}
