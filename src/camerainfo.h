#ifndef CAMERAINFO_H
#define CAMERAINFO_H

#include <QObject>

class CameraInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString deviceName READ getDeviceName WRITE setDeviceName NOTIFY deviceNameChanged)
    Q_PROPERTY(QString deviceDescription READ getDeviceDescription WRITE setDeviceDescription NOTIFY deviceDescriptionChanged)

    QString deviceName;
    QString deviceDescription;


public:    
    explicit CameraInfo(QObject *parent = nullptr);

    QString getDeviceName() const;
    void setDeviceName(const QString &value);

    QString getDeviceDescription() const;
    void setDeviceDescription(const QString &value);

signals:

    void deviceNameChanged();
    void deviceDescriptionChanged();
};

#endif // CAMERAINFO_H
