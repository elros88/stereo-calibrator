#include "threadmanager.h"

threadManager::threadManager(QObject *parent) : QObject(parent)
{

}

void threadManager::run(int device1, int device2)
{
    capture.setDevice1(device1);
    capture.setDevice2(device2);

    connect(&thread, &QThread::started,
            &capture, &Capture::stereoCalibrate);

    connect(&thread, &QThread::finished,
            &capture, &Capture::deleteLater);

    connect(&capture, &Capture::newFrame1Captured,
            this, &threadManager::receiveFrameLeft);

    connect(&capture, &Capture::newFrame2Captured,
            this, &threadManager::receivedFrameRight);

    capture.moveToThread(&thread);
    thread.start();
}

void threadManager::receiveFrameLeft(Mat frame)
{
    emit updateLeftFrame(frame);
}

void threadManager::receivedFrameRight(Mat frame)
{
    emit updateRightFrame(frame);
}

void threadManager::stopCapture()
{
    emit captureStopped();
}
