#ifndef CAMERALIST_H
#define CAMERALIST_H

#include <QAbstractListModel>
#include <QCameraInfo>
#include <QList>

#include "camerainfo.h"

class CameraList : public QAbstractListModel
{
    Q_OBJECT

    QList<CameraInfo *> list;


public:
    explicit CameraList(QObject *parent = nullptr);

    enum roles
    {
        deviceName = Qt::UserRole + 1,
        deviceDescription,
    };

    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;

    Q_INVOKABLE void cleanList();
    Q_INVOKABLE QString getDeviceName(int index);

signals:

public slots:

    Q_INVOKABLE void getCamerasInfo();

};

#endif // CAMERALIST_H
