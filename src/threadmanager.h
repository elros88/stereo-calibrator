#ifndef THREADMANAGER_H
#define THREADMANAGER_H

#include <QObject>
#include <QThread>
#include "capture.h"

class threadManager : public QObject
{
    Q_OBJECT

    QThread thread;
    Capture capture;

public:
    explicit threadManager(QObject *parent = nullptr);

    Q_INVOKABLE void run(int device1, int device2);

signals:

    void updateLeftFrame(Mat frame);
    void updateRightFrame(Mat frame);
    void captureStopped();

public slots:

    void receiveFrameLeft(Mat frame);
    void receivedFrameRight(Mat frame);
    void stopCapture();

};

#endif // THREADMANAGER_H
