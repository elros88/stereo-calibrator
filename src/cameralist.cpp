#include "cameralist.h"

#include <QDebug>

CameraList::CameraList(QObject *parent) : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> CameraList::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[deviceName] = "deviceName";
    roles[deviceDescription] = "deviceDescription";

    return roles;
}

QVariant CameraList::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= list.count())
    {
        return QVariant();
    }

    switch (role)
    {
    case deviceName:
        return list[index.row()]->getDeviceName();
    case deviceDescription:
        return list[index.row()]->getDeviceDescription();
    default:
        break;
    }

    return QVariant();
}

int CameraList::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return list.count();
}

void CameraList::getCamerasInfo()
{
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();

    cleanList();

    for(int i = 0; i<cameras.size(); i++){

        CameraInfo* cameraInfo = new CameraInfo;

        cameraInfo->setDeviceName(cameras[i].deviceName());
        cameraInfo->setDeviceDescription(cameras[i].description());
        beginInsertRows(QModelIndex(), i, i);
        list.push_back(cameraInfo);
        endInsertRows();

        cameraInfo = NULL;
        delete cameraInfo;
    }
}

void CameraList::cleanList()
{
    while(list.size()>0)
    {
        beginRemoveRows(QModelIndex(), list.size()-1, list.size()-1);
        delete list.at(list.size()-1);
        list.removeAt(list.size()-1);
        endRemoveRows();
    }
}

QString CameraList::getDeviceName(int index)
{
    return list[index]->getDeviceName();
}
