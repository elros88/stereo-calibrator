#include "camerainfo.h"


CameraInfo::CameraInfo(QObject *parent) : QObject(parent)
{

}

QString CameraInfo::getDeviceName() const
{
    return deviceName;
}

void CameraInfo::setDeviceName(const QString &value)
{
    deviceName = value;
}

QString CameraInfo::getDeviceDescription() const
{
    return deviceDescription;
}

void CameraInfo::setDeviceDescription(const QString &value)
{
    deviceDescription = value;
}
